/** Angular */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

/** Bootstrap */
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

/** Components */
import { AppComponent } from './app.component';
import { ItemsListComponent } from './items-list/items-list.component';
import { ItemComponent } from './item/item.component';
import { LabelsListComponent } from './labels-list/labels-list.component';

/** Services */
import { LoadDataService } from './app.service';

@NgModule({
  declarations: [
    AppComponent,
    ItemsListComponent,
    ItemComponent,
    LabelsListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [
    LoadDataService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
