/** Angular */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-labels-list',
  templateUrl: './labels-list.component.html',
  styleUrls: ['./labels-list.component.css']
})

export class LabelsListComponent implements OnInit {
  @Input() labelsList: string[];
  @Input() activeLabel: number;

  @Output() onSortHandler = new EventEmitter();
  @Output() onShowAllHandler = new EventEmitter();

  public sortBtn;

  constructor() { }

  ngOnInit() {
    this.sortBtn = (this.activeLabel >= 0) ? this.activeLabel : 'all';
  }

  /**
   * Emit data to parent function for sorting
   *
   * @param {Event} event Event
   * @param {String} label Name of the label
   */
  public sortHandler(event, label) {
    event.stopPropagation();
    this.onSortHandler.emit(label);
  }

  /**
   * Emit data to parent function for show all sorting
   *
   * @param {Event} event Event
   */
  public showAll(event) {
    event.stopPropagation();
    this.onShowAllHandler.emit();
  }

  /**
   * Track data by index
   *
   * @param {Number} index Index of the element
   * @returns {Number} Index of the element
   */
  public trackByIndex(index) {
    return index;
  }
}
