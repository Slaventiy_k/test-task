/** Angular */
import { Component, OnInit } from '@angular/core';

/** Services */
import { LoadDataService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  private menuList: Object[];

  public visibleMenuList: Object[];
  public sortLabels: string[];
  public activeSortLabel: number;
  public userCart: Object[];
  public editable: boolean;

  constructor(private loadDataService: LoadDataService) {
    this.editable = true;
    this.sortLabels = [];
    this.userCart = [];
  }

  ngOnInit() {
    this.loadDataService.loadMenu().subscribe(
      data => {
        this.menuList = data;
        this.visibleMenuList = this.menuList;

        this.menuList.forEach( (item) => {
          if (this.sortLabels.indexOf(item['category']) < 0) {
            this.sortLabels.push(item['category']);
          }
        });
      },
      error => console.warn(error)
    );
  }

  /**
   * Sort list by needed type
   *
   * @param {String} sortType The name of the object property by which data must be sorted
   * @param {String} typeName The name of the sort type value
   */
  public sortByType(sortType, typeName) {
    if (sortType && typeName) {
      this.visibleMenuList = this.menuList.filter((item) => {
        return item[sortType] === typeName;
      });
    } else {
      this.visibleMenuList = this.menuList;
    }
  }

  /**
   * Sort menu list by category name and set index of active label
   *
   * @param {String} name Name of the category
   */
  public sortByCategory(name) {
    this.activeSortLabel = this.sortLabels.indexOf(name);
    this.sortByType('category', name);
  }

  /**
   * Add menu item to the cart, and update summary cost of the order
   *
   * @param {Object} item Menu item which must be in the cart
   */
  public addToCart(item) {
    let addTo = false;
    this.userCart['total_price'] = 0;

    if (this.userCart.length) {
      this.userCart.forEach( order => {
        if (order['name'] === item['name']) {
          order['cnt'] ? order['cnt']++ : order['cnt'] = 2;
          order['price'] = item['price'] * order['cnt'];
          addTo = true;
        }

        this.userCart['total_price'] += order['price'];
      });
    }

    if (!addTo) {
      this.userCart.push(item);
      this.userCart['total_price'] += item['price'];
    }
  }

  /**
   * Delete item from cart, and update summary cost of the order
   *
   * @param {Object} item Menu item which must be delete from the cart
   */
  public deleteFromCart(item) {
    let idx = this.userCart.indexOf(item);
    this.userCart.splice(idx, 1);
    this.userCart['total_price'] -= item['price'];
  }
}
