/** Angular */
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class ItemComponent {
  @Input() item;
  @Input() editable;

  @Output() onEdit = new EventEmitter();
  @Output() onOrder = new EventEmitter();

  constructor() {  }

  /**
   * Emit data to parent function for deleting
   *
   * @param {Event} event Event
   */
  public deleteItem(event) {
    event.stopPropagation();
    this.onEdit.emit(this.item);
  }

  /**
   * Emit data to parent function for ordering
   *
   * @param {Event} event Event
   */
  public orderItem(event) {
    event.stopPropagation();
    this.onOrder.emit(Object.assign({}, this.item));
  }
}
