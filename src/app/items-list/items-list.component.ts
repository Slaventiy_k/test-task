/** Angular */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})

export class ItemsListComponent implements OnInit {
  @Input() itemsList;
  @Input() editable;
  @Input() sortLabels;

  @Output() onOrder = new EventEmitter();
  @Output() onEdit = new EventEmitter();

  public isEdit: boolean;

  constructor() { }

  ngOnInit() {
    this.isEdit = this.editable;
  }

  /**
   * Emit data to parent function for editing
   *
   * @param {Object} item Item which must be edited
   */
  public editAction(item) {
    this.onEdit.emit(item);
  }

  /**
   * Emit data to parent function for ordering
   *
   * @param {Object} item Item which must be edited
   */
  public orderAction(item) {
    this.onOrder.emit(item);
  }

  /**
   * Track data by index
   *
   * @param {Number} index Index of the element
   * @returns {Number} Index of the element
   */
  public trackByIndex(index) {
    return index;
  }
}
