import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()

export class LoadDataService {

  constructor(private http: Http) {}

  /**
   * Load menu
   *
   * @returns {Observable<R|T>} Response with menu
   */
  public loadMenu(): Observable<any> {
    return this.http.get('./assets/menu.json')
      .map( (response: Response) => response.json() )
      .catch( error => error );
  }
}
